from xpresso.ai.core.data.versioning.versioning_authenticator \
    import VersioningAuthenticator
from xpresso.ai.core.data.versioning.pachyderm_controller import \
    PachydermVersionController
from xpresso.ai.core.data.versioning.hdfs_version_controller import \
    HDFSVersionController


class VersionControllerFactory:
    """"""
    PACHYDERM = "pachyderm"
    HDFS = "hdfs"
    VERSIONING_CONFIG = "data_versioning"
    TOOL = "tool"
    SERVER = "server"
    HOST = "cluster_ip"
    PORT = "port"

    def __init__(self, **kwargs):
        """"""
        auth = VersioningAuthenticator(**kwargs)
        auth.authenticate_session()
        self.versioning_tool = auth.config[self.VERSIONING_CONFIG][self.TOOL]
        self.version_controller_info = self.controller_info(auth.config)
        self.kwargs = kwargs

    def get_version_controller(self, versioning_tool=PACHYDERM):
        """"""
        self.versioning_tool = versioning_tool
        if self.versioning_tool == self.PACHYDERM:
            return PachydermVersionController(self.version_controller_info,
                                              **self.kwargs)
        elif self.versioning_tool == self.HDFS:
            return HDFSVersionController(self.version_controller_info,
                                         **self.kwargs)

    def controller_info(self, config):
        """"""
        if self.versioning_tool == self.PACHYDERM:
            return {
                "name": self.versioning_tool,
                "host": config[self.VERSIONING_CONFIG][self.SERVER][self.HOST],
                "port": config[self.VERSIONING_CONFIG][self.SERVER][self.PORT]
            }
